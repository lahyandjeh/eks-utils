#!/bin/bash

if [ $# -lt 1 ]; then
   echo "Error: cluster name required."
   echo "Usage: $(basename $0) <cluster name> [region]"
   exit 1
else
   CLUSTER="$1"
   if [ -n "$2" ]; then
      REGION="$2"
   else
      REGION="`aws configure list | awk '/region/{print $2}'`"
   fi

   ENDPOINT="`aws eks describe-cluster --region $REGION --name $CLUSTER 2> /dev/null | awk '/endpoint/{print $2}' | sed 's/"//g' | sed 's/,//'`"

   echo "API ENDPOINT URL= "
   if [ -n "$ENDPOINT" ]; then
      echo $ENDPOINT
   else
      echo "Error: No endpoint found matching this cluster. Are you looking in the right region?"
      exit 1
   fi
fi
