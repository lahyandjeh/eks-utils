#!/bin/bash

if [ $# -lt 1 ]; then
   echo "Error: cluster name required."
   echo "Usage: $(basename $0) <cluster name> [region]"
   exit 1
else
   CLUSTER="$1"
   if [ -n "$2" ]; then
      REGION="$2"
   else
      REGION="`aws configure list | awk '/region/{print $2}'`"
   fi
fi

eksctl create cluster --name="$CLUSTER" --nodes=3 --node-type=t2.medium --region="$REGION" --node-private-networking
